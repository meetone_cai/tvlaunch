package com.tvbox.luanch.ui;

import android.os.Build;
import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.tvbox.luanch.BuildConfig;
import com.tvbox.luanch.base.BasePresenter;
import com.tvbox.luanch.bean.AppVersion;
import com.tvbox.luanch.bean.Respond;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LauncherPresenter extends BasePresenter<LauncherView> {
    Disposable timeCheckVersionDisposable;

    @Inject
    public LauncherPresenter() {

    }

    @Override
    public void onAttached() {

    }

    public void timeCheckVersion() {
        //每10分支更新一次数据
        Observable.interval(2, 30, TimeUnit.MINUTES)
                .observeOn(Schedulers.io())
                .subscribe(new Observer<Long>() {
                    @Override
                    public void onSubscribe(Disposable disposable) {
                        timeCheckVersionDisposable = disposable;
                    }

                    @Override
                    public void onNext(Long number) {
                        requestVersion();
                    }

                    @Override
                    public void onError(Throwable e) {
                        //取消订阅
                        timeCheckVersionCancel();
                    }

                    @Override
                    public void onComplete() {
                        //取消订阅
                        timeCheckVersionCancel();
                    }
                });
    }
    /**
     * 取消订阅
     */
    public void timeCheckVersionCancel() {
        if (timeCheckVersionDisposable != null && !timeCheckVersionDisposable.isDisposed()) {
            timeCheckVersionDisposable.dispose();
        }
    }

    public void requestVersion() {
        String serial = Build.SERIAL;
        if (BuildConfig.DEBUG) {
            serial = "EMULATOR29X2X1X0";
        }
        Disposable disposable = requestStore.requestVersion(serial)
                .doOnSuccess(respond -> {
                    if (respond.isOk()) {
                        String body = respond.getBody();
                        if (!TextUtils.isEmpty(body)) {
                            try {
                                AppVersion shopList = JSON.parseObject(body, AppVersion.class);
                                respond.setObj(shopList);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(respond -> {
                            mView.checkUpdateCallback(respond);
                        },
                        throwable -> {
                            Respond respond = getThrowableRespond(throwable);
                            mView.checkUpdateCallback(respond);
                        });
        mCompositeSubscription.add(disposable);
    }
}
