package com.tvbox.luanch.ui;

import com.tvbox.luanch.base.BaseView;
import com.tvbox.luanch.bean.AppVersion;
import com.tvbox.luanch.bean.Respond;

public interface LauncherView extends BaseView {
    void checkUpdateCallback(Respond<AppVersion> respond);
}
