package com.tvbox.luanch.base;

import com.tvbox.luanch.bean.DownFile;
import com.tvbox.luanch.bean.MyObjectBox;

import io.objectbox.Box;
import io.objectbox.BoxStore;

public class DataManager {

    private static DataManager dataManager;

    public static synchronized DataManager getInstance() {
        if (dataManager == null) {
            dataManager = new DataManager();
        }
        return dataManager;
    }

    private DataManager() {
    }

    public BoxStore boxStore;
    public Box<DownFile> downFileBox;

    public void init(App app) {
        this.boxStore = MyObjectBox.builder().androidContext(app).build();
        downFileBox = boxStore.boxFor(DownFile.class);
    }
}
