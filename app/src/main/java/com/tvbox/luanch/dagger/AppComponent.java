package com.tvbox.luanch.dagger;

import com.tvbox.luanch.LauncherActivity;

import dagger.Component;

/**
 * Created by clarence on 2018/3/26
 */
@ActivityScope
@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(LauncherActivity launcherActivity);
}
