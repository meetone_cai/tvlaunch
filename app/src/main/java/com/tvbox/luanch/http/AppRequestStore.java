package com.tvbox.luanch.http;

import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tvbox.luanch.bean.ErrorRespond;
import com.tvbox.luanch.bean.Respond;
import com.tvbox.luanch.bean.SuccessRespond;

import java.util.Map;

import io.reactivex.Single;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;

public class AppRequestStore {

    Retrofit retrofit;

    private static class Holder {
        private static final AppRequestStore instance = new AppRequestStore();
    }

    public static AppRequestStore getInstance() {
        return Holder.instance;
    }

    private AppRequestStore() {
        this.retrofit = HttpManager.getInstance().retrofit;
    }

    public Function<ResponseBody, Respond> getCommonFunction() {
        return responseBody -> {
            Respond respondDO = new ErrorRespond();
            try {
                String result = responseBody.string();
                if (!TextUtils.isEmpty(result) && !"[]".equals(result)) {
                    JSONObject jsonObject = JSONObject.parseObject(result);
                    if (jsonObject != null) {
                        String error = jsonObject.getString("error");
                        //说明是错误的
                        if (!TextUtils.isEmpty(error) && error.contains("{") && error.contains("}")) {
                            respondDO = JSON.parseObject(result, ErrorRespond.class);
                        } else if (jsonObject.getIntValue("error") == 0) {
                            respondDO = JSON.parseObject(result, SuccessRespond.class);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respondDO;
        };
    }

    /**
     * post 参数封装
     *
     * @return
     */
    public RequestBody getRequestBody(String value) {
        if (TextUtils.isEmpty(value)) {
            return null;
        }
        return RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), value);
    }

    /**
     * 参数转json
     *
     * @param params
     * @return
     */
    public String getJson(Map<String, Object> params) {
        if (params == null || params.size() == 0) {
            return null;
        }
        JSONObject jsonObject = new JSONObject();
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            jsonObject.put(entry.getKey(), entry.getValue());
        }
        return jsonObject.toJSONString();
    }

    /**
     * post 参数封装
     *
     * @param params
     * @return
     */
    public RequestBody getRequestBody(Map<String, Object> params) {
        if (params == null || params.size() == 0) {
            return null;
        }
        String jsonString = getJson(params);
        return getRequestBody(jsonString);
    }

    /**
     * 请求二维码
     *
     * @return
     */
    public Single<Respond> requestQrCode() {
        return retrofit.create(ApiService.class)
                .requestQrCode()
                .map(getCommonFunction())
                .subscribeOn(Schedulers.io());
    }

    /**
     * 请求版本更新
     *
     * @return
     */
    public Single<Respond> requestVersion(String serial) {
        return retrofit.create(ApiService.class)
                .requestVersion(serial)
                .map(getCommonFunction())
                .subscribeOn(Schedulers.io());
    }
}
