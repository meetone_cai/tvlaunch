package com.tvbox.luanch.http;

import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * 接口声明
 */
public interface ApiService {

    @Streaming
    @GET
    Observable<ResponseBody> download(@Url String url);//直接使用网址下载

    @GET("getDesktopVersion/{serial}")
    Single<ResponseBody> requestVersion(@Path("serial") String serial);

    @POST("sendMessage")
    Single<ResponseBody> sendMessage(@Body RequestBody requestBody);

    @GET("generate_image_code")
    Single<ResponseBody> generateImageCode();

    @GET("generate_qrcode")
    Single<ResponseBody> requestQrCode();

    @POST("saveEquipment")
    Single<ResponseBody> saveEquipment(@Body RequestBody requestBody);
}
