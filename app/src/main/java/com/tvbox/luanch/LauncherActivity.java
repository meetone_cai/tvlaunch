package com.tvbox.luanch;

import android.Manifest;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.owen.focus.FocusBorder;
import com.tvbox.luanch.base.App;
import com.tvbox.luanch.base.BaseActivity;
import com.tvbox.luanch.base.BasePresenter;
import com.tvbox.luanch.base.Constant;
import com.tvbox.luanch.bean.AppVersion;
import com.tvbox.luanch.bean.Respond;
import com.tvbox.luanch.databinding.LauncherTvBinding;
import com.tvbox.luanch.http.ProgressInfo;
import com.tvbox.luanch.http.ProgressListener;
import com.tvbox.luanch.http.ProgressManager;
import com.tvbox.luanch.ui.LauncherPresenter;
import com.tvbox.luanch.ui.LauncherView;
import com.tvbox.luanch.utils.SystemUtils;
import com.tvbox.luanch.utils.ToastUtils;
import com.tvbox.luanch.utils.Utils;
import com.tvbox.luanch.view.AppUpdateDialog;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;

public class LauncherActivity extends BaseActivity<LauncherTvBinding> implements LauncherView {

    @Inject
    LauncherPresenter launcherPresenter;
    FocusBorder mFocusBorder;
    private final int GET_PERMISSION_REQUEST = 100; //权限申请自定义码
    AppUpdateDialog dialog;
    String downloadPath;

    @Override
    public void addPresenters(List<BasePresenter> observerList) {
        observerList.add(launcherPresenter);
    }

    @Override
    public int getLayoutId() {
        return R.layout.launcher_tv;
    }

    @Override
    public void initDagger() {
        App.getAppComponent().inject(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            finish();
            return;
        }
    }

    @Override
    public void initView() {
        mFocusBorder = new FocusBorder.Builder()
                .asColor()
                .borderColorRes(R.color.actionbar_color)
                .borderWidth(TypedValue.COMPLEX_UNIT_DIP, 3f)
                .shadowColorRes(R.color.green_bright)
                .shadowWidth(TypedValue.COMPLEX_UNIT_DIP, 5f)
                .build(this);
        mFocusBorder.setVisible(true);

        LinearLayout llGoIn = findViewById(R.id.llGoIn);
        llGoIn.setOnFocusChangeListener((v, hasFocus) -> onMoveFocusBorder(v, 1.1f));
        llGoIn.setOnClickListener(v -> {
            String packageName = "com.easy.tvbox";
            boolean hasIn = SystemUtils.isAppExist(LauncherActivity.this, packageName);
            if (hasIn) {
                SystemUtils.openApp(LauncherActivity.this, packageName);
            } else {
                ToastUtils.showShort("应用还未安装，请先安装应用");
            }
        });

        LinearLayout llGoSet = findViewById(R.id.llGoSet);
        llGoSet.setOnFocusChangeListener((v, hasFocus) -> onMoveFocusBorder(v, 1.1f));
        llGoSet.setOnClickListener(v -> {
            //com.droidlogic.FileBrower
            Intent intent = new Intent(Settings.ACTION_SETTINGS);
            startActivity(intent);
        });

        onMoveFocusBorder(llGoIn, 1.1f);
        getPermissions();
        String fileName = "tvLaunch.apk";
        downloadPath = Utils.getSaveFilePath(Constant.TYPE_APP, this) + fileName;
        launcherPresenter.timeCheckVersion();
    }

    protected void onMoveFocusBorder(View focusedView, float scale) {
        if (null != mFocusBorder) {
            mFocusBorder.onFocus(focusedView, FocusBorder.OptionsFactory.get(scale, scale));
        }
    }

    /**
     * 获取权限
     */
    private boolean getPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                //不具有获取权限，需要进行权限申请
                ActivityCompat.requestPermissions(LauncherActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, GET_PERMISSION_REQUEST);
                return false;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == GET_PERMISSION_REQUEST) {
            if (grantResults.length >= 1) {
//                int writeResult = grantResults[0];
                //读写内存权限
//                boolean writeGranted = writeResult == PackageManager.PERMISSION_GRANTED;//读写内存权限
            }
        }
    }

    @Override
    public void networkChange(boolean isConnect) {

    }

    private void showAppVersionDialog(AppVersion appVersion) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        dialog = new AppUpdateDialog(this);
        dialog.show();
        ProgressManager.getInstance().addResponseListener(appVersion.getDownloadUrl(), getDownloadListener());
        ProgressManager.getInstance().startDownload(downloadPath, appVersion.getDownloadUrl());
    }

    private ProgressListener getDownloadListener() {
        return new ProgressListener() {
            @Override
            public void onProgress(ProgressInfo progressInfo) {
                int progress = progressInfo.getPercent();
                Log.d("DownLoad", "--Download-- " + progress + " %  " + progressInfo.getSpeed() + " byte/s  " + progressInfo.toString());
                if (dialog != null) {
                    dialog.setProgress(progress);
                }
                if (progressInfo.isFinish()) {
                    if (dialog != null) {
                        dialog.setProgress(100);
                        dialog.dismiss();
                    }
                    if (downloadPath != null) {
                        Utils.install(mContext, BuildConfig.APPLICATION_ID, downloadPath);
                    }
                }
            }

            @Override
            public void onError(long id, Exception e) {
                e.printStackTrace();
            }
        };
    }

    @Override
    public void checkUpdateCallback(Respond<AppVersion> respond) {
        if (respond.isOk()) {
            AppVersion appVersion = respond.getObj();
            if (appVersion != null) {
                int currentVersion = SystemUtils.getVersion(BuildConfig.VERSION_NAME);
                int versionName = currentVersion;
                if (!TextUtils.isEmpty(appVersion.getVersion())) {
                    versionName = SystemUtils.getVersion(appVersion.getVersion());
                }
                if (versionName > currentVersion) {
                    showAppVersionDialog(appVersion);
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        launcherPresenter.timeCheckVersionCancel();
        super.onDestroy();
    }
}
